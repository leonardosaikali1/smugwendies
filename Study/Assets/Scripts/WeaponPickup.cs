﻿using UnityEngine;
using System.Collections;

public class WeaponPickup : MonoBehaviour
{
  public GameObject pickup;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }

  void OnTriggerEnter2D(Collider2D col)
  {
    //print("VVVVVVVVVVVVVVVV");
    if(col.GetComponent<PlayerController>() != null)
    {
      col.gameObject.SendMessage("EquipWeapon", pickup);
    }
  }
}
