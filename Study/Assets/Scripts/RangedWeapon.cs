﻿using UnityEngine;
using System.Collections;

public class RangedWeapon : Weapon
{
  public GameObject Projectile;
  public tk2dSpriteAnimator SpecialEffectAnimator;

  void Attack(Vector3 attackVec)
  {
    //Don't attack twice
    if (GetAttacking()) return;

    var attack = AttackBasic(attackVec);

    if (attack != null)
    {
      attack.layer = LayerMask.NameToLayer("Weapon");
    }

    GameObject proj = (GameObject) Instantiate(Projectile, attack.transform.position, new Quaternion(0,0,0,0));

    SpecialEffectAnimator.Play();

    proj.GetComponent<Projectile>()._user = user;
    proj.GetComponent<Projectile>()._weapon = this;
    proj.GetComponent<Projectile>()._pindex = _pindex;
    proj.SendMessage("Kickoff", attackVec);

  }
}
