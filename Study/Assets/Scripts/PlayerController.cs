﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour {

  /*Character Stats*/
  public int playerIndex = 0;
  public float maxSpeed = 2f;
  public float moveForce = 365f;
  public float jumpForce = 365f;
  public float gravity = 10f;

  /*XInput Controller Stuff*/

  private PlayerIndex playerID;
  //The previous state of the gamepad.
  private GamePadState prevState;
  //The current state of the gamepad.
  private GamePadState curState;

  /*Character Controller Stuff*/
  CharacterController2D1 _controller;
  bool controls_frozen = false;
  public bool controls_frozen_perm = false;

  //The object we parent our guns to; we rotate this to aim.
  [SerializeField]
  private GameObject WeaponAnchor;
  private Vector3 AimDirection;

  /*Weapon Stuff*/
  public GameObject EquippedWeapon;
  GameObject CurrentWep;

  //Weapon Sprite
  [SerializeField]
  private tk2dSprite WeaponSprite;
  [SerializeField]
  private tk2dSpriteAnimator WeaponSpriteAnimator;

  //Animation stuff
  [SerializeField]
  CharacterAnimationController PlayerAnimController;
  Facing curFacing;

  // Use this for initialization
  void Start ()
  {
    curFacing = Facing.Right;

    Camera.main.SendMessage("AddObject", gameObject);
    _controller = GetComponent<CharacterController2D1>();
  }

  Vector3 GetMouseRelative()
  {
    Vector3 v3 = Input.mousePosition;
    v3 = Camera.main.ScreenToWorldPoint(v3);

    v3.z = 0.0f;
    v3 -= gameObject.transform.position;

    return v3;
  }

  void EquipWeapon(GameObject eqi)
  {
    WeaponSprite.SetSprite(eqi.GetComponent<tk2dSprite>().spriteId);
    WeaponSpriteAnimator.DefaultClipId = eqi.GetComponent<tk2dSpriteAnimator>().DefaultClipId;

    if (CurrentWep != null && CurrentWep.GetComponent<Weapon>().GetAttacking()) return;

    if (CurrentWep != null) Destroy(CurrentWep);
    EquippedWeapon = eqi;
    CurrentWep = Instantiate(EquippedWeapon, WeaponAnchor.transform);

    var wep = CurrentWep.GetComponent<Weapon>();
    wep.user = gameObject;
    wep._pindex = playerIndex;
  }

  // Will call bulk functions
  void Update()
  {
    CombatInput();
    MovementInput();

    curState = GamePad.GetState(playerID);

    AimDirection = new Vector3(curState.ThumbSticks.Right.X, curState.ThumbSticks.Right.Y, 0);

    if(AimDirection == Vector3.zero && PlayerAnimController.GetAnimState() == AnimState.Idle)
    {
      if (curFacing == Facing.Left)
      {
        AimDirection = new Vector3(-1, 0, 0);
      }

      else if (curFacing == Facing.Right)
      {
        AimDirection = new Vector3(1, 0, 0);
      }
    }

    WeaponAnchor.transform.right = AimDirection;
  }

  // Checks for combat inputs
  void CombatInput()
  {
    if (CurrentWep == null) return;

    if (curState.Triggers.Right > 0.75)
    {
      CurrentWep.SendMessage("Attack", AimDirection, SendMessageOptions.RequireReceiver);
      WeaponSpriteAnimator.Play();
    }
  }

  //Does movement input
  void MovementInput()
  {
    var velocity = _controller.velocity;

    if (_controller.isGrounded) velocity.x = 0;

    velocity = DoKnockbackChanges(velocity);
    velocity = DoInputChanges(velocity);

    if (Input.GetKeyDown(KeyCode.M))
    {
      Knockback(3, new Vector3(1, 1, 0));
    }

    velocity.y += -gravity * Time.deltaTime;

    _controller.move(velocity * Time.deltaTime);
  }

  // Change move vector based on inputs
  Vector3 DoInputChanges(Vector3 velocity)
  {
    if (controls_frozen || controls_frozen_perm) return velocity;

    Vector3 move = new Vector3(curState.ThumbSticks.Left.X, 0);

    if (move.x != 0 && !_controller.isGrounded)
    {
      if (velocity.x <= maxSpeed && move.x > 0)
      {
        velocity.x += move.x * moveForce * Time.deltaTime * 10;
      }
      if (velocity.x >= -maxSpeed && move.x < 0)
      {
        velocity.x += move.x * moveForce * Time.deltaTime * 10;
        print(velocity.x);
      }
    }
    else if(move.x != 0)
    {
      //checking for moving left, then changing sprite to face that direction.
      if(move.x < 0 && curFacing == Facing.Right)
      {
        PlayerAnimController.FlipSprite();
        curFacing = Facing.Left;
      }
      if(move.x > 0 && curFacing == Facing.Left)
      {
        PlayerAnimController.FlipSprite();
        curFacing = Facing.Right;
      }

      velocity.x = move.x * moveForce;

      PlayerAnimController.SendMessage("ChangeAnimState", AnimState.Run);
    }
    else if(_controller.isGrounded)
      velocity.x = 0;

    if (curState.Triggers.Left > 0.75 && _controller.isGrounded)
    {
      velocity.y = jumpForce;

      PlayerAnimController.SendMessage("ChangeAnimState", AnimState.Jump);
    }

    if(move.x == 0)
    {
      PlayerAnimController.SendMessage("ChangeAnimState", AnimState.Idle);
    }

    return velocity;
  }

  bool Knocked = false;
  // Change move vector based on knockback
  Vector3 DoKnockbackChanges(Vector3 velocity)
  {
    if (Knocked)
    {
      Knocked = false;
      velocity = StoredKnockback;
    }
    else
    {
      velocity += StoredKnockback * Time.deltaTime;
    }

    StoredKnockback.x -= Time.deltaTime * gravity * 10000;
    StoredKnockback.y -= Time.deltaTime * gravity * 10000;

    if (StoredKnockback.x <= 0) StoredKnockback.x = 0;
    if (StoredKnockback.y <= 0) StoredKnockback.y = 0;

    return velocity;
  }


  // Store a knockback upon being hit
  Vector3 StoredKnockback;
  void Knockback(float force, Vector3 source)
  {
    Knocked = true;
    IEnumerator ko = freezeControls(0.3f);
    StartCoroutine(ko);

    source = gameObject.transform.position - source;

    source.y = Mathf.Abs(source.y);
    source.y += (Mathf.Abs(source.x) + source.y)/2;

    source.Normalize();
    source *= force;

    StoredKnockback = source;
  }

  void OnTakeKnockback(DamagePacket pack)
  {
    Knockback(pack._knockback_value, pack._source.transform.position);
  }

  IEnumerator freezeControls(float time)
  {
    controls_frozen = true;
    yield return new WaitForSeconds(time);
    controls_frozen = false;
  }
}
