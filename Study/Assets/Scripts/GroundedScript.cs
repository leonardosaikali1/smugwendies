﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedScript : MonoBehaviour {

	// Use this for initialization
	void Start ()
  {
		
	}
	
  void OnTriggerEnter2D()
  {
    gameObject.GetComponentInParent<AimScript>().isGrounded = true;
    print("TRUE");
  }

  void OnTriggerExit2D()
  {
    gameObject.GetComponentInParent<AimScript>().isGrounded = false;
    print("FALSE");
  }

	// Update is called once per frame
	void Update ()
  {
		
	}
}
