﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
  public float Speed;
  public bool Arcs;

  public GameObject _user;
  public RangedWeapon _weapon;
  public int _pindex;

  [SerializeField]
  GameObject BulletDeathParticle;

  void Kickoff(Vector3 dir)
  {
    dir.Normalize();

    GetComponent<Rigidbody2D>().gravityScale = 0;

    GetComponent<Rigidbody2D>().AddForce(dir * Speed, ForceMode2D.Impulse);
  }

  void OnTriggerEnter2D(Collider2D col)
  {
    if (col.gameObject == _weapon) return;
    if (col.gameObject.GetComponent<PlayerController>() != null && col.gameObject.GetComponent<PlayerController>().playerIndex == _pindex) return;
    
    col.gameObject.SendMessage("OnTakeDamage", new DamagePacket(_user, gameObject, _weapon.Damage, _weapon.Knockback));

    print(col);

    GameObject particle = (GameObject)Instantiate(BulletDeathParticle, transform.position, new Quaternion(0, 0, 0, 0));

    Destroy(gameObject);
  }
}
