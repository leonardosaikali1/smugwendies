﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

  [SerializeField]
  private float MaxHP;

  private float CurHP;

	// Use this for initialization
	void Start ()
  {
    CurHP = MaxHP;
	}

  void OnTakeDamage(DamagePacket packet)
  {
    //gameObject.SendMessage("OnTakeKnockback", packet);

    CurHP -= packet._damage_value;

    UpdateSprite();

    gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 1);
  }

  void UpdateSprite()
  {
    if (gameObject.GetComponent<tk2dSprite>() != null)
    {

    }
  }

	// Update is called once per frame
	void Update () {
	
	}
}
