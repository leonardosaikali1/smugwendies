﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Requiring tk2dsprite
[RequireComponent(typeof(tk2dSprite))]
//Requiring tk2dspriteanimator
[RequireComponent(typeof(tk2dSpriteAnimator))]

/*This script defines a particle that plays on default (see tk2dspriteanimator) and destroys itself after 
 the animation is complete.*/
public class ParticleBehavior : MonoBehaviour {

  tk2dSpriteAnimator particlespriteanimator;
  tk2dSpriteAnimationClip particledeathclip;

  // Use this for initialization
  void Start ()
  {
    particlespriteanimator = gameObject.GetComponent<tk2dSpriteAnimator>();
    particledeathclip = particlespriteanimator.CurrentClip;

    //After animation is done, destroy it.
    particlespriteanimator.AnimationCompleted += Die;
  }

  void Die (tk2dSpriteAnimator animator, tk2dSpriteAnimationClip clip)
  {
    Destroy(gameObject);
  }

	// Update is called once per frame
	void Update ()
  {
	}
}
