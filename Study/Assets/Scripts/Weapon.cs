﻿using UnityEngine;
using System.Collections;

public enum AnimType
{
  Spin,
  Thrust,
  Nothing
}

public class Weapon : MonoBehaviour
{
  public Sprite wepsprite;
  public GameObject user;

  public float Damage;
  public float Knockback;
  public float Speed;
  public float Cooldown;
  public int _pindex;

  bool Attacking = false;

  public bool GetAttacking() { return Attacking; }

  // Tells the controller how to animate it (Always points at mouse)
  public AnimType animType;

  public void Start()
  {
    if (Speed > Cooldown) Cooldown = Speed;
  }

  public GameObject AttackBasic(Vector3 attackVec)
  {
    //Create weapon
    //Animate weapon
    //Return the weapon in case anything needs it. 

    GameObject obj = new GameObject();

    obj.AddComponent<SpriteRenderer>();
    obj.GetComponent<SpriteRenderer>().sprite = wepsprite;

    obj.transform.parent = user.transform;
    obj.transform.localPosition = new Vector3(0, 0, 0);

    if (animType == AnimType.Spin)
    {
      obj.transform.localPosition += new Vector3(0, 0.1f, 0);

      if (attackVec.x <= 0)
        StartCoroutine(SwingLeft(obj.transform, user.transform));
      else
        StartCoroutine(SwingRight(obj.transform, user.transform));

    }

    attackVec.Normalize();

    if(animType == AnimType.Thrust)
    {
      obj.transform.localPosition += attackVec * 0.1f;
      StartCoroutine(StabDirection(obj.transform, attackVec));
    }

    if (animType == AnimType.Nothing)
    {
      obj.transform.localPosition += attackVec * 0.1f;
      StartCoroutine(NoAnim(obj.transform, attackVec));
    }

    return obj;
  }


  //Spins weapon this way <--
  IEnumerator SwingLeft(Transform target, Transform user)
  {
    Attacking = true;
    print("Attacking.");

    float time = 0;

    while (time < Speed)
    {
      time += Time.deltaTime;

      var angle = 120 / Speed;

      target.RotateAround(user.position, new Vector3(0, 0, 1), Mathf.Deg2Rad * angle);

      yield return null;
    }

    Destroy(target.gameObject);

    yield return new WaitForSeconds(Cooldown - Speed);

    Attacking = false;
    print("Can Attack Again.");
  }

  //Spins weapon this way -->
  IEnumerator SwingRight(Transform target, Transform user)
  {
    Attacking = true;
    print("Attacking.");

    float time = 0;

    while (time < Speed)
    {
      time += Time.deltaTime;

      var angle = -120 / Speed;

      target.RotateAround(user.position, new Vector3(0, 0, 1), Mathf.Deg2Rad * angle);

      yield return null;
    }

    Destroy(target.gameObject);

    yield return new WaitForSeconds(Cooldown - Speed);
    
    Attacking = false;
    print("Can Attack Again.");
  }

  IEnumerator StabDirection(Transform target, Vector3 dir)
  {
    Attacking = true;
    print("Attacking.");

    Vector3 diff = dir - target.localPosition;
    diff.Normalize();

    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
    target.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

    float time = 0;

    while (time < Speed / 2)
    {
      time += Time.deltaTime;

      target.localPosition += (diff * Time.deltaTime);

      yield return null;
    }

    while (time < Speed)
    {
      time += Time.deltaTime;

      target.localPosition -= (diff * Time.deltaTime);

      yield return null;
    }

    Destroy(target.gameObject);

    yield return new WaitForSeconds(Cooldown - Speed);

    Attacking = false;
    print("Can Attack Again.");
  }

  IEnumerator NoAnim(Transform target, Vector3 dir)
  {
    Attacking = true;
    print("Attacking.");

    Vector3 diff = dir - target.localPosition;
    diff.Normalize();

    float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
    target.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

    yield return new WaitForSeconds(Speed);

    Destroy(target.gameObject);

    yield return new WaitForSeconds(Cooldown - Speed);

    Attacking = false;
    print("Can Attack Again.");
  }
}
