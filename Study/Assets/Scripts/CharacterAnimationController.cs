﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimState { Idle, Jump, Run, Dead }

public enum Facing { Left, Right }

public class CharacterAnimationController : MonoBehaviour {

  //The animators for each separate part
  [SerializeField]
  tk2dSpriteAnimator HeadAnimator;
  [SerializeField]
  tk2dSpriteAnimator BodyAnimator;

  AnimState curAnimState;

  // Use this for initialization
  void Start ()
  {
    curAnimState = AnimState.Idle;
	}

	// Update is called once per frame
	void Update ()
  {
	}

  //Flips the sprite on the y axis 180 degrees to switch between moving left and right.
  public void FlipSprite()
  {
    gameObject.transform.Rotate(0, 180, 0);
  }

  public AnimState GetAnimState()
  {
    return curAnimState;
  }

  //Function is passed the desired animation state to change to.
  public void ChangeAnimState(AnimState newState)
  {
    switch(newState)
    {
      case AnimState.Idle:
        {
          HeadAnimator.Play("Idle");
          BodyAnimator.Play("Body_Idle_Handless");
          break;
        }

      case AnimState.Jump:
        {
          HeadAnimator.Play("Jump");
          BodyAnimator.Play("Body_Jump_Handless");
          break;
        }

      case AnimState.Run:
        {
          HeadAnimator.Play("Run");
          BodyAnimator.Play("Body_Run_Handless");
          break;
        }

      case AnimState.Dead:
        {
          HeadAnimator.Play("Run");
          BodyAnimator.Play("Body_Dead_Handless");
          break;
        }
    }
  }
}
