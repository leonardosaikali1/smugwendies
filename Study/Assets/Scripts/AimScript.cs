﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

//This PlayerController uses XInput in order to move players. 
public class AimScript : MonoBehaviour {

  private enum MovementState { Idle, MoveRight, MoveLeft, Jumping }

  private MovementState curMoveState;

  private Vector2 MovementVector;

  //What player this is according to the controller.
  //private PlayerIndex playerID;

  //What player this is in-game. (Should be the same as above???)
  private int playerIndex;

  //The previous state of the gamepad.
  private GamePadState prevState;
  //The current state of the gamepad.
  private GamePadState curState;

  //The object we parent our guns to; we rotate this to aim.
  [SerializeField]
  private GameObject WeaponAnchor;

  public GameObject EquippedWeapon;
  GameObject CurrentWep;

  public bool isGrounded; 

  // Use this for initialization
  void Start ()
  {
    //Camera.main.SendMessage("AddObject", gameObject);
  }

  /*
  Vector3 GetMouseRelative()
  {
    Vector3 v3 = Input.mousePosition;
    v3 = Camera.main.ScreenToWorldPoint(v3);

    v3.z = 0.0f;
    v3 -= gameObject.transform.position;

    return v3;
  }
  */

  /*
  void EquipWeapon(GameObject eqi)
  {
    if (CurrentWep != null && CurrentWep.GetComponent<Weapon>().GetAttacking()) return;

    if (CurrentWep != null) Destroy(CurrentWep);
    EquippedWeapon = eqi;
    CurrentWep = Instantiate(EquippedWeapon);

    var wep = CurrentWep.GetComponent<Weapon>();
    wep.user = gameObject;
    wep._pindex = playerIndex;
  }
  */

  /*
  void ChangeMovementState(MovementState newMoveState)
  {
    switch (newMoveState)
    {
      //Not doing anything
      case MovementState.Idle:
        {
          return;
        }
      case MovementState.Jumping:
        {
          return;
        }

      case MovementState.MoveLeft:
        {
          return;
        }

      case MovementState.MoveRight:
        {
          return;
        }
    }

  }
  */

  //Physics
  void FixedUpdate ()
  {
    if(isGrounded == false)
    {
      //gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -gravity));

      print("HEY");
    }
    else if(isGrounded == true)
    {
      gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
      gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
    }
  }

	// Update is called once per frame
	void Update ()
  {
    //curState = GamePad.GetState(playerID);
    /*
    if(curState.ThumbSticks.Left.X > -0.1 && curState.ThumbSticks.Left.X < 0.1)
    {
      ChangeMovementState(MovementState.Idle);
      print("IDLE");
    }
    */

    /*Jumping*/
    /*
    if (curState.Triggers.Left >= 0.75)
    {
      print("JUMP");
    }
    */

    /*Shooting*/
    /*
    if(curState.Triggers.Right >= 0.75)
    {
      print("SHOOT");
    }
    */

    /*Movement*/
    /*
    if (curState.ThumbSticks.Left.X < -0.25)
    {
      ChangeMovementState(MovementState.MoveLeft);
      print("MOVING LEFT");
    }

    if(curState.ThumbSticks.Left.X > 0.25)
    {
      ChangeMovementState(MovementState.MoveRight);
      print("MOVING RIGHT");
    }
    */

    /*Aiming*/
    WeaponAnchor.transform.right = new Vector3(curState.ThumbSticks.Right.X, curState.ThumbSticks.Right.Y, 0);

    print(WeaponAnchor.transform.right);
	}
}
