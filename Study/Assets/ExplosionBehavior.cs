﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehavior : MonoBehaviour {

  [SerializeField]
  GameObject ExplosionDeathParticle;

  [SerializeField]
  int Damage;

  [SerializeField]
  int Knockback;

  [SerializeField]
  private bool DestroyAfterTime;

  [SerializeField]
  float maxTime;
  float curTime;

  // Use this for initialization
  void Start ()
  {
    curTime = maxTime;
	}

  void OnTriggerEnter2D(Collider2D col)
  {
    col.gameObject.SendMessage("OnTakeDamage", new DamagePacket(gameObject, gameObject, Damage, Knockback));

    Die();
  }

  void Die()
  {
    GameObject particle = (GameObject)Instantiate(ExplosionDeathParticle, transform.position, new Quaternion(0, 0, 0, 0));

    Destroy(gameObject);
  }

  // Update is called once per frame
  void Update ()
  {
		if(DestroyAfterTime == true)
    {
      curTime -= Time.deltaTime;

      if(curTime <= 0)
      {
        Die();
      }
    }
	}
}
