﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockType { Brick, Bomb, Crate, Permanent, Random}

public class BlockBehavior : MonoBehaviour {

  //maximum hp value
  [SerializeField]
  private float MaxHP;
  //current hp value
  private float CurHP;

  public BlockType chosenBlockType;

  tk2dTiledSprite BlockSprite;

  [SerializeField]
  private GameObject Drop;

  // Use this for initialization
  void Start ()
  {
    BlockSprite = gameObject.GetComponent<tk2dTiledSprite>();

    //Randomly choosing a blocktype. 
    if (chosenBlockType == BlockType.Random)
    {
      int chosenblockval = (int)Mathf.Round(Random.Range(0, 4));

      switch (chosenblockval)
      {
        case 0:
          {
            chosenBlockType = BlockType.Brick;
            BlockSprite.SetSprite(3);
            MaxHP = 3;
            break;
          }
        case 1:
          {
            chosenBlockType = BlockType.Brick;
            BlockSprite.SetSprite(4);
            MaxHP = 2;
            break;
          }
        case 2:
          {
            chosenBlockType = BlockType.Brick;
            BlockSprite.SetSprite(5);
            MaxHP = 1;
            break;
          }
        case 3:
          {
            chosenBlockType = BlockType.Bomb;
            BlockSprite.SetSprite(6);
            MaxHP = 1;
            Drop = (GameObject) Resources.Load("Explosion");
            break;
          }
      }
    }

    CurHP = MaxHP;
  }

  void OnTakeDamage(DamagePacket packet)
  {
    if(chosenBlockType == BlockType.Permanent)
    {
      return;
    }

    //gameObject.SendMessage("OnTakeKnockback", packet);

    CurHP -= packet._damage_value;

    UpdateBlockState();
  }

  void UpdateBlockState()
  {
    //Pretty horribly hard-coded right now, but eh...
    if(chosenBlockType == BlockType.Brick)
    {
      if(CurHP == 2)
      {
        BlockSprite.SetSprite(4);
      }
      else if(CurHP == 1)
      {
        BlockSprite.SetSprite(5);
      }
    }
    
    if(CurHP <= 0)
    {
      Die();
    }
  }

  void Die()
  {
    if(chosenBlockType == BlockType.Bomb)
    {
      GameObject explosionhitbox = (GameObject)Instantiate(Drop, transform.position, new Quaternion(0, 0, 0, 0));
    }

    Destroy(gameObject);
  }

  // Update is called once per frame
  void Update () {
		
	}
}
